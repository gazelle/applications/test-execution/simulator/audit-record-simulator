package fr.ceoche.jcainboundexample;

import javax.mail.Message;

public interface MailMessageListener {
    public void onMessage(Message message);
}
