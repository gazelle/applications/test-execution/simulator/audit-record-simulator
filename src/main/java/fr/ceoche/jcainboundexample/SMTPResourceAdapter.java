package fr.ceoche.jcainboundexample;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import javax.resource.NotSupportedException;
import javax.resource.ResourceException;
import javax.resource.spi.ActivationSpec;
import javax.resource.spi.BootstrapContext;
import javax.resource.spi.ConfigProperty;
import javax.resource.spi.Connector;
import javax.resource.spi.ResourceAdapter;
import javax.resource.spi.ResourceAdapterInternalException;
import javax.resource.spi.endpoint.MessageEndpointFactory;
import javax.resource.spi.work.Work;
import javax.resource.spi.work.WorkException;
import javax.resource.spi.work.WorkManager;
import javax.transaction.xa.XAResource;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Connector(displayName = "Mail Adapter example",
        vendorName = "Acme Solutions, Inc",
        eisType = "SMTP server",
        version = "1.0")
public class SMTPResourceAdapter implements ResourceAdapter, Work {

    private static final Logger LOG = LoggerFactory.getLogger(SMTPResourceAdapter.class);

    // adapter config attributes
    @ConfigProperty(type = Integer.class, defaultValue = "25")
    private int port = 25;

    // adapter attributes
    private ServerSocketChannel ssc = null;
    private Selector selector = null;
    private WorkManager workManager;
    private Map<ActivationSpec, MessageEndpointFactory> factories = new HashMap<>();

    // Work attributes
    private boolean isRunning = false;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    final synchronized boolean isRunning() {
        return isRunning;
    }

    final synchronized void setRunning(boolean running) {
        isRunning = running;
    }

    public Map<ActivationSpec, MessageEndpointFactory> getFactories() {
        return factories;
    }

    public void start(BootstrapContext ctx) throws ResourceAdapterInternalException {
        try {
            ssc = ServerSocketChannel.open();
            ssc.configureBlocking(false);
            InetAddress localhost = InetAddress.getLocalHost();
            InetSocketAddress smtpLocal = new InetSocketAddress(localhost, port);
            ssc.socket().bind(smtpLocal);
        } catch (IOException e) {
            throw new ResourceAdapterInternalException("Unable to start SMTP server socket", e);
        }

        try {
            selector = SelectorProvider.provider().openSelector();
            ssc.register(selector, SelectionKey.OP_ACCEPT);
        } catch (IOException e) {
            throw new ResourceAdapterInternalException("Unable to register Selector for SMTP server socket", e);
        }

        try {
            workManager = ctx.getWorkManager();
            workManager.startWork(this);
        } catch (WorkException e) {
            throw new ResourceAdapterInternalException("Unable to start SMTP work", e);
        }

    }

    public void stop() {
        release();
    }

    public void endpointActivation(MessageEndpointFactory endpointFactory, ActivationSpec spec) throws ResourceException {
        if (!(spec instanceof SMTPActivationSpec)) {
            throw new NotSupportedException("Invalid activation spec");
        }
        factories.put(spec, endpointFactory);
    }

    public void endpointDeactivation(MessageEndpointFactory endpointFactory, ActivationSpec spec) {
        factories.remove(spec);
    }

    public XAResource[] getXAResources(ActivationSpec[] specs) throws ResourceException {
        return new XAResource[0];
    }

    public void run() {
        setRunning(true);
        try {
            while (isRunning()) {
                if (selector.select() > 0) {
                    Set<SelectionKey> i = selector.selectedKeys();
                    SelectionKey sk = (SelectionKey) i.iterator().next();
                    i.remove(sk);

                    ServerSocketChannel sscReady = (ServerSocketChannel) sk.channel();
                    SocketChannel channel = sscReady.accept();
                    workManager.scheduleWork(new SMTPProcessor(this, channel));
                }
            }
        } catch (WorkException | IOException e) {
            LOG.error("Error while waiting/reading from SMTP server socket", e);
        }
    }

    public void release() {
        setRunning(false);
        selector.wakeup();
    }
}
