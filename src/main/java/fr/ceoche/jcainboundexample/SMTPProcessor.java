package fr.ceoche.jcainboundexample;

import javax.resource.spi.ActivationSpec;
import javax.resource.spi.endpoint.MessageEndpointFactory;
import javax.resource.spi.work.Work;
import java.net.InetAddress;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

public class SMTPProcessor implements Work {

    private SMTPResourceAdapter smptResourceAdapter ;
    private SocketChannel channel ;

    public SMTPProcessor(SMTPResourceAdapter smtpResourceAdapter, SocketChannel channel) {
        this.smptResourceAdapter = smtpResourceAdapter;
        this.channel = channel;
    }

    @Override
    public void run() {
        // channel.read()

        Iterator<ActivationSpec> i = smptResourceAdapter.getFactories().keySet().iterator();
        while(i.hasNext()) {
            SMTPActivationSpec spec = (SMTPActivationSpec) i.next();
            if(spec.accepts(InetAddress.getByName("127.0.0.1"))) {
                MessageEndpointFactory factory = smptResourceAdapter.getFactories().get(spec);
                MailMessageListener mailMessageListener = (MailMessageListener) factory.createEndpoint(null);
                //mailMessageListener.onMessage(msg);
            }
        }
    }

    @Override
    public void release() {

    }
}
