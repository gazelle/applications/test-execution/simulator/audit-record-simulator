package fr.ceoche.jcainboundexample;

import javax.ejb.ActivationConfigProperty;
import javax.resource.ResourceException;
import javax.resource.spi.Activation;
import javax.resource.spi.ActivationSpec;
import javax.resource.spi.ConfigProperty;
import javax.resource.spi.InvalidPropertyException;
import javax.resource.spi.ResourceAdapter;
import java.io.Serializable;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

@Activation(messageListeners = { MailMessageListener.class })
public class SMTPActivationSpec implements ActivationSpec, Serializable {

    @ConfigProperty(type = String.class)
    private String expression = null;

    private Pattern pattern = null;

    private SMTPResourceAdapter smtpResourceAdapter;

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    @Override
    public ResourceAdapter getResourceAdapter() {
        return smtpResourceAdapter;
    }

    @Override
    public void setResourceAdapter(ResourceAdapter resourceAdapter) throws ResourceException {
        this.smtpResourceAdapter = (SMTPResourceAdapter) resourceAdapter;
    }

    @Override
    public void validate() throws InvalidPropertyException {
        try {
            pattern = Pattern.compile(expression) ;
        } catch (PatternSyntaxException e) {
            throw new InvalidPropertyException("Invalid pattern syntax", e);
        }
    }

    boolean accepts(String recipientAddress) throws InvalidPropertyException {
        if(pattern == null) {
            validate();
        }
        return pattern.matcher(recipientAddress).matches();
    }
}
