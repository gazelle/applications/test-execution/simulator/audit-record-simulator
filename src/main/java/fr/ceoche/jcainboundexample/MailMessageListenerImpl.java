package fr.ceoche.jcainboundexample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJBException;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.mail.Message;
import javax.transaction.Transactional;

@MessageDriven(
        name = "mailMessageListener",
        mappedName = "tcp/mail",
        messageListenerInterface = MailMessageListener.class)
@Transactional(value = Transactional.TxType.NOT_SUPPORTED)
public class MailMessageListenerImpl implements MailMessageListener, MessageDrivenBean {

    private static final long serialVersionUID = 3003662706919754899L;
    private static final Logger LOG = LoggerFactory.getLogger(MailMessageListenerImpl.class);

    public void setMessageDrivenContext(MessageDrivenContext ctx) throws EJBException {

    }

    public void ejbRemove() throws EJBException {

    }

    public void onMessage(Message message) {

    }

}
