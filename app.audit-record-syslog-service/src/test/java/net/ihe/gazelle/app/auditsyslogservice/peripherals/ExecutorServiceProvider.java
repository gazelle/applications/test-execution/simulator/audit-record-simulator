package net.ihe.gazelle.app.auditsyslogservice.peripherals;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Encapsulate a thread {@link ExecutorService} for Unit tests
 * <p>
 *
 * @author ceoche
 */
public class ExecutorServiceProvider {

   private static final Logger LOG = LoggerFactory.getLogger(ExecutorServiceProvider.class);

   private volatile static ExecutorService executorService = null;

   private ExecutorServiceProvider() {
   }

   public synchronized static void execute(Runnable runnable) {
      if (executorService == null) {
         executorService = Executors.newCachedThreadPool();
      }
      executorService.execute(runnable);
   }

   public synchronized static void shutdown() {
      if (executorService != null) {
         LOG.info("gazelle-syslog shutdown in progress...");
         executorService.shutdownNow();
         try {
            if (!executorService.awaitTermination(5, TimeUnit.SECONDS)) {
               LOG.error("gazelle-syslog executor failed to terminate");
            } else {
               LOG.info("gazelle-syslog shutdown completed");
            }
            executorService = null;
         } catch (InterruptedException ie) {
            LOG.error("InterruptedException thrown durring executor shutdown, interrupt thread.");
            executorService.shutdownNow();
            executorService = null;
            Thread.currentThread().interrupt();
         }
      }
   }



}
