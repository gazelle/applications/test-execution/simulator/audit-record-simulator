package net.ihe.gazelle.app.auditsyslogservice.adapter;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

public class RawSyslogMessage {

    private byte[] rawMessage;
    private TransportType transportType;
    private InetSocketAddress senderAddress;
    private InetSocketAddress recieverAddress;

    public RawSyslogMessage(byte[] rawMessage, TransportType transportType, InetSocketAddress senderAddress, InetSocketAddress recieverAddress) {
        this.rawMessage = rawMessage;
        this.transportType = transportType;
        this.senderAddress = senderAddress;
        this.recieverAddress = recieverAddress;
    }

    public byte[] getRawMessage() {
        return rawMessage;
    }

    public TransportType getTransportType() {
        return transportType;
    }

    public InetSocketAddress getSenderAddress() {
        return senderAddress;
    }

    public InetSocketAddress getRecieverAddress() {
        return recieverAddress;
    }
}
