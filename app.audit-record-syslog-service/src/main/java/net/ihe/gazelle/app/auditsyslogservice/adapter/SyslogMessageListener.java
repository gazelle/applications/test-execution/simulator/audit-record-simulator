package net.ihe.gazelle.app.auditsyslogservice.adapter;

public interface SyslogMessageListener {

    void receiveRawSyslogMessage(RawSyslogMessage rawSyslogMessage);

}
