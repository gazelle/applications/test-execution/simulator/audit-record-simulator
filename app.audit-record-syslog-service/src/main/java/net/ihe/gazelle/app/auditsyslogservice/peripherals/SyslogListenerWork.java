package net.ihe.gazelle.app.auditsyslogservice.peripherals;

import net.ihe.gazelle.app.auditsyslogservice.adapter.RawSyslogMessage;
import net.ihe.gazelle.app.auditsyslogservice.adapter.SyslogMessageListener;

import javax.resource.spi.work.Work;

public class SyslogListenerWork implements Work {

   SyslogMessageListener syslogMessageListener ;
   RawSyslogMessage rawSyslogMessage ;

   SyslogListenerWork(SyslogMessageListener syslogMessageListener, RawSyslogMessage rawSyslogMessage) {
      this.rawSyslogMessage = rawSyslogMessage;
      this.syslogMessageListener = syslogMessageListener;
   }

   @Override
   public void release() {

   }

   @Override
   public void run() {
      syslogMessageListener.receiveRawSyslogMessage(rawSyslogMessage);
   }

}
