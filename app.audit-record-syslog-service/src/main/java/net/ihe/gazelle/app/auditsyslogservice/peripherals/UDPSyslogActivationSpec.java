package net.ihe.gazelle.app.auditsyslogservice.peripherals;

import net.ihe.gazelle.app.auditsyslogservice.adapter.RawSyslogMessage;
import net.ihe.gazelle.app.auditsyslogservice.adapter.SyslogMessageListener;

import javax.resource.ResourceException;
import javax.resource.spi.Activation;
import javax.resource.spi.ActivationSpec;
import javax.resource.spi.ConfigProperty;
import javax.resource.spi.InvalidPropertyException;
import javax.resource.spi.ResourceAdapter;
import java.net.InetSocketAddress;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

@Activation(messageListeners = {SyslogMessageListener.class})
public class UDPSyslogActivationSpec implements ActivationSpec {

   @ConfigProperty(type = String.class, defaultValue = "^$", description = "Ignore messages that have sender's IP or hostname that matches the " +
         "given regex")
   private String regExpAddressFilteredOut;

   @ConfigProperty(type = Boolean.class, defaultValue = "true", description = "Ignore UDP datagram packet recieved that have no data content. " +
         "Ignored packet will not be recorded by the simulator")
   private Boolean ignoreEmptyPacket;

   private Pattern pattern;
   private UDPSyslogResourceAdapter udpSyslogResourceAdapter;

   public UDPSyslogActivationSpec() {
   }

   public ResourceAdapter getResourceAdapter() {
      return udpSyslogResourceAdapter;
   }

   public void setResourceAdapter(ResourceAdapter ra) throws ResourceException {
      if (ra instanceof UDPSyslogResourceAdapter) {
         this.udpSyslogResourceAdapter = (UDPSyslogResourceAdapter) ra;
      } else {
         throw new ResourceException(String.format("{} is expected to be associated with {}", this.getClass().getCanonicalName(),
               UDPSyslogResourceAdapter.class.getCanonicalName()));
      }
   }

   public void validate() throws InvalidPropertyException {
      try {
         pattern = Pattern.compile(regExpAddressFilteredOut);
      } catch (PatternSyntaxException e) {
         throw new InvalidPropertyException("Invalid pattern syntax for property regExpAddressFilteredOut", e);
      }
   }

   public boolean accepts(RawSyslogMessage rawSyslogMessage) {
      return isPacketSizeOK(rawSyslogMessage) && isSenderAddressOK(rawSyslogMessage.getSenderAddress());
   }

   private boolean isSenderAddressOK(InetSocketAddress senderAddress) {
      return not(pattern.matcher(senderAddress.getHostName()).matches()) &&
            not(pattern.matcher(senderAddress.getAddress().getHostAddress()).matches());
   }

   private boolean isPacketSizeOK(RawSyslogMessage rawSyslogMessage) {
      return not(ignoreEmptyPacket && rawSyslogMessage.getRawMessage().length == 0);
   }

   private boolean not(boolean expression) {
      return !expression;
   }

}
