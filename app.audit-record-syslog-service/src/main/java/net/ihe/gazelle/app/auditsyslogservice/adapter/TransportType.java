package net.ihe.gazelle.app.auditsyslogservice.adapter;

/**
 * Created by cel on 11/01/17.
 */
public enum TransportType {

    TCP("TCP", "RFC 6587"),
    TCP_TLS("TCP-TLS", "RFC 5425"),
    UDP("UDP", "RFC 5426");

    private String friendlyName;
    private String standard;

    private TransportType(String friendlyName, String standard) {
        this.friendlyName = friendlyName;
        this.standard = standard;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public String getStandard() {
        return standard;
    }

    public String toString() {
        return getFriendlyName();
    }

}
