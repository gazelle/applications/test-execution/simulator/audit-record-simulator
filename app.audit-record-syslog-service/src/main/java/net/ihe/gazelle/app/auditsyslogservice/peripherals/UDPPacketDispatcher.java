package net.ihe.gazelle.app.auditsyslogservice.peripherals;

import java.net.DatagramPacket;
import java.net.InetAddress;

public interface UDPPacketDispatcher {

   void dispatchPacket(DatagramPacket packet) ;

}
