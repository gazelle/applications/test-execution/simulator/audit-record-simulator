package net.ihe.gazelle.app.auditsyslogservice.peripherals;

import net.ihe.gazelle.app.auditsyslogservice.adapter.RawSyslogMessage;
import net.ihe.gazelle.app.auditsyslogservice.adapter.SyslogMessageListener;
import net.ihe.gazelle.app.auditsyslogservice.adapter.TransportType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.resource.ResourceException;
import javax.resource.spi.ActivationSpec;
import javax.resource.spi.BootstrapContext;
import javax.resource.spi.ConfigProperty;
import javax.resource.spi.Connector;
import javax.resource.spi.ResourceAdapter;
import javax.resource.spi.ResourceAdapterInternalException;
import javax.resource.spi.TransactionSupport;
import javax.resource.spi.UnavailableException;
import javax.resource.spi.endpoint.MessageEndpointFactory;
import javax.resource.spi.work.WorkException;
import javax.resource.spi.work.WorkManager;
import javax.transaction.xa.XAResource;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@Connector(
      displayName = "UDP Syslog Adapter",
      vendorName = "IHE Europe",
      eisType = "ATNA SA/SN SUT",
      transactionSupport = TransactionSupport.TransactionSupportLevel.NoTransaction
)
public class UDPSyslogResourceAdapter implements ResourceAdapter, UDPPacketDispatcher {

   private static final Logger LOG = LoggerFactory.getLogger(UDPSyslogResourceAdapter.class);

   @ConfigProperty(type = Integer.class, defaultValue = "3001", description = "Port on which the UDP Server is listening")
   private Integer port;

   @ConfigProperty(type = Integer.class, defaultValue = "1500", description = "Idle period (milliseconds) on which the server is waiting for " +
         "incoming packets. The server will check potential thread interruption everytime the period is over")
   private Integer idleTimePeriod;

   @ConfigProperty(type = Integer.class, defaultValue = "8192", description = "Datagram packet max length (bytes) for reading incomming messages. " +
         "Minimum is 480 and 1180 bytes for respectivly IPv4 and IPv6 Syslog receivers. Recommended is 2048 bytes. Longer is encouraged. Max " +
         "allowed by " +
         " protocol is 65535 bytes, but may increase risk of packet loss. Default value is 8192 bytes.")
   private Integer datagramMaxLength;

   @ConfigProperty(type = String.class, defaultValue = "localhost", description = "Host name or IP address of this UDP server. Used as information " +
         "in recording only")
   private String hostAddress;

   private InetSocketAddress simulatorAddress;
   private Map<ActivationSpec, MessageEndpointFactory> factories = new ConcurrentHashMap<>();
   private WorkManager workManager;
   private UDPSocketMessageProvider udpSocketMessageProvider;

   @Override
   public void start(BootstrapContext ctx) throws ResourceAdapterInternalException {
      workManager = ctx.getWorkManager();
      simulatorAddress = InetSocketAddress.createUnresolved(hostAddress, port);
      udpSocketMessageProvider = new UDPSocketMessageProvider(simulatorAddress, idleTimePeriod, datagramMaxLength, this);
      try {
         workManager.scheduleWork(udpSocketMessageProvider);
      } catch (WorkException e) {
         LOG.error("Unable to start UDP Socket Message Provider", e);
      }
   }

   @Override
   public void stop() {
      udpSocketMessageProvider.release();
   }

   @Override
   public void endpointActivation(MessageEndpointFactory endpointFactory, ActivationSpec spec) throws ResourceException {
      if (spec instanceof UDPSyslogActivationSpec) {
         spec.validate();
         factories.put(spec, endpointFactory);
      } else {
         throw new ResourceException(String.format("Unsupported activation spec %s", spec.getClass().getCanonicalName()));
      }
   }

   @Override
   public void endpointDeactivation(MessageEndpointFactory endpointFactory, ActivationSpec spec) {
      factories.remove(spec);
   }

   @Override
   public XAResource[] getXAResources(ActivationSpec[] specs) throws ResourceException {
      return new XAResource[0];
   }

   @Override
   public void dispatchPacket(DatagramPacket packet) {

      RawSyslogMessage rawSyslogMessage = new RawSyslogMessage(packet.getData(), TransportType.UDP,
            new InetSocketAddress(packet.getAddress(), packet.getPort()), simulatorAddress);

      for (Map.Entry<ActivationSpec, MessageEndpointFactory> factory : factories.entrySet()) {
         if (((UDPSyslogActivationSpec) factory.getKey()).accepts(rawSyslogMessage)) {
            try {
               SyslogListenerWork syslogWork = new SyslogListenerWork((SyslogMessageListener) factory.getValue().createEndpoint(null),
                     rawSyslogMessage);
               workManager.scheduleWork(syslogWork);
            } catch (WorkException | UnavailableException e) {
               LOG.error("Unable to dispatch UDP Message to the Syslog Listener", e);
            }
         }
      }

   }

   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (o == null || getClass() != o.getClass()) {
         return false;
      }
      UDPSyslogResourceAdapter that = (UDPSyslogResourceAdapter) o;
      return Objects.equals(port, that.port) &&
            Objects.equals(idleTimePeriod, that.idleTimePeriod) &&
            Objects.equals(datagramMaxLength, that.datagramMaxLength) &&
            Objects.equals(hostAddress, that.hostAddress);
   }

   @Override
   public int hashCode() {
      return Objects.hash(port, idleTimePeriod, datagramMaxLength, hostAddress);
   }

}

