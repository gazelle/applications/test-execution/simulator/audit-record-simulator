package net.ihe.gazelle.app.auditsyslogservice.peripherals;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.resource.spi.work.Work;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;

/**
 * <p>
 * This is an UDP Datagram Packet Server. It corresponds to the EIS Message Provider in the JEE JCA model.
 * </p>
 * <p>
 * This UDP server is a Work (a.k.a. a Runnable manageable by a WorkManager), It will listen incomming datagram for a given period of time and check
 * regularly if it as recieve the order to stop (order to stop can be given by a call to {@link #release()}.
 * </p>
 * <p>
 * Every incomming datagram packets received will be forwarded to the given {@link #UDPPacketDispatcher}. The callback to the dispatcher is performed
 * in the same thread than the UDP Server is listening, so if the packet processing is too long, it may be preferable that the dispatcher create new
 * {@link Work} to perform the packet processing.
 * </p>
 */
public class UDPSocketMessageProvider implements Work {

   private static final Logger LOG = LoggerFactory.getLogger(UDPSocketMessageProvider.class);

   private final InetSocketAddress SOCKET_ADDRESS;
   private final int LISTENING_TIME_PERIOD;
   private final int PACKET_MAX_LENGTH;


   private net.ihe.gazelle.app.auditsyslogservice.peripherals.UDPPacketDispatcher UDPPacketDispatcher;
   private volatile boolean isAllowedToRun = true;

   /**
    * {@code UDPSocketMessageProvider} Constructor.
    *
    * @param port                port the UDP server socket will listen to.
    * @param listeningTimePeriod listening time in millisecond the server socket will remain waiting for incomming message before checking if this
    *                            work has receive the order to stop. This period must not be too long or it will prevent the WorkManager to
    *                            efficiently manage this thread. (should be from 300 to 3000 ms)
    * @param packetMaxLength     Datagram packet max length allowed (bytes) for reading incomming messages.
    * @param udpPacketDispatcher dispatcher to which the {@code UDPSocketMessageProvider} will forward recieved datagram paquets.
    */
   public UDPSocketMessageProvider(InetSocketAddress socketAddress, int listeningTimePeriod, int packetMaxLength,
                                   net.ihe.gazelle.app.auditsyslogservice.peripherals.UDPPacketDispatcher udpPacketDispatcher) {
      this.SOCKET_ADDRESS = socketAddress;
      this.LISTENING_TIME_PERIOD = listeningTimePeriod;
      this.PACKET_MAX_LENGTH = packetMaxLength;
      this.UDPPacketDispatcher = udpPacketDispatcher;
   }

   /**
    * Check whether the UDP Socket is still allowed to continue running
    *
    * @return true if the UDP socket Message Provider can carry on listening incoming message, false if it has recieve the order from the WorkManager
    * to stop.
    */
   public boolean isAllowedToRun() {
      return isAllowedToRun;
   }

   /**
    * Ask the UDPSocket to stop listening message and terminate its execution. This method is non-blocking and the UDPSocket may not be terminated
    * when exiting the method.
    * <p>
    * {@inheritDoc}
    */
   public void release() {
      isAllowedToRun = false;
   }

   /**
    * Start the UDP server socket on the configured port. This is a blocking method. The method will resumes each time the {@code listeningTimePeriod}
    * is out or when a packet is recieved. Recieved packets are forwarded to the configured listener. The method will loop until an unexepcted error
    * happens or until the {@link #release()} method is called.
    * <p>
    * {@inheritDoc}
    */
   public void run() {

      try (DatagramSocket datagramSocket = new DatagramSocket(SOCKET_ADDRESS)) {
         datagramSocket.setSoTimeout(LISTENING_TIME_PERIOD);
         LOG.info("UDP server listening on PORT: {}", SOCKET_ADDRESS.getPort());

         while (isAllowedToRun()) {
            try {
               // Block in this call until a packet is receive or socket timeout is reached
               UDPPacketDispatcher.dispatchPacket(recievePacket(datagramSocket));
            } catch (SocketTimeoutException ste) {
               LOG.trace("UDP server (PORT {}) socket timeout reached, perform Work loop", SOCKET_ADDRESS.getPort());
               // Will loop again if Work release has not been asked.
            }
         }

      } catch (IOException ioe) {
         LOG.error("UDP server (PORT {}) unexpected error : {}", SOCKET_ADDRESS.getPort(), ioe.getMessage());
      } finally {
         LOG.info("UDP server (PORT {}) interrupted, will halt now...", SOCKET_ADDRESS.getPort());
         return;
      }

   }

   private DatagramPacket recievePacket(DatagramSocket datagramSocket) throws SocketTimeoutException, IOException {

      byte[] buffer = new byte[PACKET_MAX_LENGTH];
      DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

      LOG.trace("UDP server (PORT {}) waiting for incomming packet...", SOCKET_ADDRESS.getPort());
      // Block at this point until a packet is receive or socket timeout is reached
      datagramSocket.receive(packet);

      return packet;

   }

}
